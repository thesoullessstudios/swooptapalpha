﻿using UnityEngine;
using System.Collections;

public class Unlock : MonoBehaviour {
	public AnimationClip GET_CAPSULE;
	public AnimationClip SHOW_CAPSULE;
	public Selectable[] selectables;
	public SpriteRenderer PostIt;
	public SpriteRenderer Unlockable;
	public Sprite Try;
	public Sprite New;
	public GameObject display;
	public GameObject leave;
	public int whatever;
	
	// Update is called once per frame
	void Update () {

		if(Input.GetKeyDown(KeyCode.L)){
			Test();
		}
	}
	void Unlocker(){
		StartCoroutine(SHOW_CHAR_UNLOCKED());
		int unlocked = Random.Range(1,selectables.Length);
		int isUnlocked = 1;
		if (PlayerPrefs.GetInt (selectables [unlocked].storedName) != isUnlocked) {
			PlayerPrefs.SetInt (selectables [unlocked].storedName, isUnlocked);
			PostIt.sprite = New;
			Unlockable.sprite = selectables[unlocked].reg;
		} else {
			PostIt.sprite = Try;
			Unlockable.sprite = selectables[unlocked].reg;
		}

	}
	void Test(){

		PlayerPrefs.SetInt(selectables [whatever].storedName, 1);
	}
	IEnumerator SHOW_CHAR_UNLOCKED(){
		yield return new WaitForSeconds(SHOW_CAPSULE.length + GET_CAPSULE.length + 1);
		display.SetActive(true);
		leave.SetActive(true);
	}
}
