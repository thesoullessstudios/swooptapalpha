﻿using UnityEngine;
using System.Collections;

public class AssetSpawner : MonoBehaviour {
	public PLayerBehaviour[] objects;
	public GameObject BG_Spawner;
	public string selected;
	public GameObject xception;
	// Use this for initialization
	void Start () {
	 selected = PlayerPrefs.GetString ("Selected");
		if (selected != "") {
			for(int i = 0;i < objects.Length; i ++){
				if(selected == objects[i].Name){
					Instantiate(objects[i].gameObject,transform.position,transform.rotation);
					GameObject groundBack = Instantiate(objects[i].BG,BG_Spawner.transform.position,BG_Spawner.transform.rotation) as GameObject;
					if(objects[i].BG == xception){
						groundBack.transform.position = new Vector3 (BG_Spawner.transform.position.x - 1.5f,BG_Spawner.transform.position.y - 1.5f,BG_Spawner.transform.position.z);
					}
					break;
				}
				if(i == objects.Length && selected != objects[i].Name){
					selected = "";
				}
			}

		}
		if(selected == ""){
			Instantiate(objects[0].gameObject,transform.position,transform.rotation);
			Instantiate(objects[0].BG,BG_Spawner.transform.position,BG_Spawner.transform.rotation);
		}
	}

}
