﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {
    public float ProjectileSpeed;
	public int TIME_BEFORE_DEATH = 5;
	private bool TIMER_STARTED = false;
	// Use this for initialization
	void Start () {


	
	}
	
	// Update is called once per frame
	void Update () {

        transform.Translate(Vector2.left * (ProjectileSpeed * Time.deltaTime));
        if(!TIMER_STARTED){
			StartCoroutine(DEATH_TIMER());
			TIMER_STARTED = true;
		}

    }
	IEnumerator DEATH_TIMER(){

		yield return new WaitForSeconds(TIME_BEFORE_DEATH);
		Destroy(this.gameObject);
	}
}
