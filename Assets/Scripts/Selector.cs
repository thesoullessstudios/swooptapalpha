﻿using UnityEngine;
using System.Collections;

public class Selector : MonoBehaviour {

	public Vector3 stored;
	public Vector2 startPos;
	public Vector2 direction;
	public Vector3 dir;
	public bool dirChosen;
	public GameObject selectionBox;
	public float waitForDir = 5f;
	public float storedWait;
	public float second;
	 public bool up = false;
	public bool timeToLoad = false;
	public float waitForAnim = 125f;

	// Use this for initialization
	void Start () {
		storedWait = waitForDir;
	}
	
	// Update is called once per frame
	void Update () {

		second = Time.deltaTime * 60;
		Vector3 v3  = Input.mousePosition;
		/*if (Input.touchCount == 1) {
			var touch = Input.GetTouch(0);
			switch(touch.phase){
			case TouchPhase.Began:
				startPos.x = touch.position.x;
				dirChosen = false;
				break;
			case TouchPhase.Moved:
				direction.x = touch.position.x - startPos.x;
				break;
			
			}
		}
		selectionBox.transform.Translate (direction.x * Time.deltaTime, 0f, 0f);*/
		if (timeToLoad) {
			waitForAnim -= second;
			if(waitForAnim <= 0){
				Application.LoadLevel("DaBaseGame");
			}
		}
		if (up) {
			waitForDir -= second;
			if(waitForDir <= 0){
				dir.x = dir.x / 1000f * Time.deltaTime;
			}
		}
		if (Input.GetMouseButtonDown (0)) {
			up = false;
			waitForDir = storedWait;
			v3 = Camera.main.ScreenToWorldPoint (v3);
			stored.x = v3.x;
		}
		if (Input.GetMouseButton (0)) {
			v3 = Camera.main.ScreenToWorldPoint (v3);
			dir.x = v3.x - stored.x;
		}
		selectionBox.transform.Translate (dir.x * Time.deltaTime, 0f, 0f);
		if (Input.GetMouseButtonUp (0)) {
			up = true;


		}


	}
	void Load(){
		timeToLoad = true;
	}
}
