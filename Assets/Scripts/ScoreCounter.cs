﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreCounter : MonoBehaviour {
	public BalloonHitDetection detect;

	// Use this for initialization
	void Start () {
		detect = GameObject.FindWithTag ("Finish").GetComponent<BalloonHitDetection>();
	}
	
	// Update is called once per frame
	void Update () {
		Text me = GetComponent<Text> ();
		me.text = "" + detect.score;
	}
}
