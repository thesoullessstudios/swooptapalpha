﻿using UnityEngine;
using System.Collections;

public class PoofSpawn : MonoBehaviour {
	public AnimationClip DISSAPATE;
	private float poofSpeedX;
	private float poofSpeedY;
	public Sprite[] Poofs;
	private bool HAS_STARTED_DEATH = false;
	// Use this for initialization
	void Start () {
		poofSpeedX = Random.Range (-1.5f, 1.5f);
		poofSpeedY = Random.Range (-1.5f, 1.5f);
		int randoPoof = Random.Range (0, 5);
		SpriteRenderer me = GetComponent<SpriteRenderer> ();
		me.sprite = Poofs [randoPoof];

	}
	
	// Update is called once per frame
	void Update () {


		transform.Translate (poofSpeedX * Time.deltaTime, poofSpeedY * Time.deltaTime, 0f);
		if(!HAS_STARTED_DEATH && gameObject.activeInHierarchy){
			HAS_STARTED_DEATH = true;
			StartCoroutine(ON_DEATH());
		}
	}
	IEnumerator ON_DEATH(){
		yield return new WaitForSeconds(DISSAPATE.length);
		gameObject.SetActive(false);
		transform.position = GameObject.FindWithTag("Pooler").transform.position;
		HAS_STARTED_DEATH = false;
	}
}
