﻿using UnityEngine;
using System.Collections;

public class BalloonSpawning : MonoBehaviour {
	public GameObject Balloon;
	public float timeBetweenBalloons = 10f;
	public ObjectPooler pool;
	private bool IS_READY = true;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	if(IS_READY){

			IS_READY = false;
			StartCoroutine(SPAWN_BALLOON());
		}

	}
	IEnumerator SPAWN_BALLOON(){
		int BALLOONS_ACTIVE = 0;
		for(int i = 0; i < pool.BALLOON_POOL.Length; i++){
			if(pool.BALLOON_POOL[i].activeInHierarchy == false){
				pool.BALLOON_POOL[i].transform.position = new Vector3(transform.position.x,Random.Range(-5,2.5f),transform.position.z);
				pool.BALLOON_POOL[i].SetActive(true);
				break;
			}else{

				BALLOONS_ACTIVE++;
			}

		}
		if(BALLOONS_ACTIVE == pool.BALLOONS_ALLOTED){
			GameObject[] TEMP_POOL = new GameObject[pool.BALLOONS_ALLOTED + 1];
			pool.BALLOON_POOL.CopyTo(TEMP_POOL,0);
			pool.BALLOON_POOL = TEMP_POOL;
			pool.BALLOONS_ALLOTED = pool.BALLOONS_ALLOTED + 1;
			pool.BALLOON_POOL[pool.BALLOON_POOL.Length - 1] = (GameObject) Instantiate(pool.BALLOON,transform.position,transform.rotation);
			pool.BALLOON_POOL[pool.BALLOON_POOL.Length - 1].SetActive(true);
		}
		yield return new WaitForSeconds(timeBetweenBalloons);
		IS_READY = true;
	}
}
