﻿using UnityEngine;
using System.Collections;

public class BGMoveScript : MonoBehaviour {
	public float bgMoveSpeed = 1;
	public float queueBackX = -16.5f;
	public float queueReturnX = 25.5f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate (-1f * bgMoveSpeed * Time.deltaTime, 0f, 0f);
		if (transform.position.x < queueBackX) {
			transform.position = new Vector3(queueReturnX,transform.position.y,0f);
		}
	}
}
