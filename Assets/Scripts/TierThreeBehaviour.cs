﻿using UnityEngine;
using System.Collections;

public class TierThreeBehaviour : MonoBehaviour {
    public GameObject Projectile;
	public float ANIM_SHOOT_SPEED;
    public float EnemySpeed;
    public float EnemyProjectileTimer;
    public bool hasShot = false;
    public AnimationClip charge;
	private int MAX_SHOTS;

	// Use this for initialization
	void Start ()
    {
        EnemySpeed = Random.Range(1.5f, 4f);
        EnemyProjectileTimer = Random.Range(1.5f, 2.5f);
		MAX_SHOTS = Random.Range (0,4);

	}
	
	// Update is called once per frame
	void Update ()
    {
        transform.Translate(Vector2.left * (EnemySpeed * Time.deltaTime));
        if (transform.position.x <= GameObject.FindGameObjectWithTag("Player").transform.position.x - 10)
        {
			gameObject.SetActive(false);
			transform.position = GameObject.FindWithTag("Pooler").transform.position;
			hasShot = false;
			MAX_SHOTS = Random.Range (0,4);
		}
		if (!hasShot && gameObject.activeInHierarchy && MAX_SHOTS < 3)
        {
            hasShot = true;
            StartCoroutine(Shoot());

        }
        
        
	}

    IEnumerator Shoot()
    {
        yield return new WaitForSeconds(EnemyProjectileTimer);
        GetComponent<Animator>().SetInteger("State", 1);
		GetComponent<Animator>().speed = ANIM_SHOOT_SPEED;
        EnemySpeed = 0;
        StartCoroutine(ProjectileSpawn());



    }

    IEnumerator ProjectileSpawn()
    {
        yield return new WaitForSeconds((charge.length / ANIM_SHOOT_SPEED) - .01f);
        GameObject PROJECTILE_ONE = Instantiate(Projectile, new Vector3(transform.position.x - .5f, transform.position.y, transform.position.z), transform.rotation) as GameObject;
		GameObject PROJECTILE_TWO = Instantiate(Projectile, new Vector3(transform.position.x - .5f, transform.position.y, transform.position.z), transform.rotation) as GameObject;
		PROJECTILE_ONE.gameObject.transform.eulerAngles = new Vector3(0,0,25);
		PROJECTILE_TWO.gameObject.transform.eulerAngles = new Vector3(0,0,-25);
		StartCoroutine(ANIM_RESET());

    }
	IEnumerator ANIM_RESET(){

		yield return new WaitForSeconds(charge.length / ANIM_SHOOT_SPEED - .5f);
		GetComponent<Animator>().SetInteger("State", 0);
		GetComponent<Animator>().speed = 1f;
		EnemySpeed = Random.Range(1.5f, 4f);
		hasShot = false;
		MAX_SHOTS++;
	}
}
