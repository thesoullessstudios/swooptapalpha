﻿using UnityEngine;
using System.Collections;

public class LoadBaseGame : MonoBehaviour {
	public AnimationClip LEAVE_SCENE_ANIM;




	void LoadBase(){
		StartCoroutine(ON_LEAVE_SCENE());

	}
	IEnumerator ON_LEAVE_SCENE(){
		yield return new WaitForSeconds(LEAVE_SCENE_ANIM.length + .1f);
		Application.LoadLevel("DaBaseGame");
	}
}
