﻿using UnityEngine;
using System.Collections;

public class ObjectPooler : MonoBehaviour {
	public int BALLOONS_ALLOTED;
	public int ADV_ENEMIES_ALLOTED;
	public int ENEMIES_ALLOTED;
	public int COINS_ALLOTED;
	public int POOFS_ALLOTED;
	public GameObject[] BALLOON_POOL;
	public GameObject[] ENEMY_POOL;
	public GameObject[] COIN_POOL;
	public GameObject[] ADV_ENEMY_POOL;
	public GameObject[] POOF_POOL;
	public GameObject BALLOON;
	public GameObject ENEMY;
	public GameObject COIN;
	public GameObject POOF;
	// Use this for initialization
	void Start () {
		BALLOON_POOL = new GameObject[BALLOONS_ALLOTED];
		for(int i = 0; i < BALLOON_POOL.Length; i++){
			BALLOON_POOL[i] = (GameObject) Instantiate(BALLOON,transform.position,transform.rotation);
			BALLOON_POOL[i].SetActive(false);
		}
		ENEMY_POOL = new GameObject[ENEMIES_ALLOTED];
		for(int i = 0; i < ENEMY_POOL.Length; i++){
			ENEMY_POOL[i] = (GameObject) Instantiate(ENEMY,transform.position,transform.rotation);
			ENEMY_POOL[i].SetActive(false);
		}
		POOF_POOL = new GameObject[POOFS_ALLOTED];
		for(int i = 0; i < POOF_POOL.Length; i++){
			POOF_POOL[i] = (GameObject) Instantiate(POOF,transform.position,transform.rotation);
			POOF_POOL[i].SetActive(false);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
